import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from DB import *

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()
        self.bdd = DB()

    def create_widgets(self):
        self.hi_there = tk.Button(self)
        self.hi_there["text"] = "Hello World\n(click me)"
        self.hi_there["command"] = self.say_hi
        self.hi_there.pack(side="top")

        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=self.master.destroy)
        self.quit.pack(side="bottom")

        self.tabBar = ttk.Notebook(root)
        self.createImp()
        self.createExp()
        self.createAjout()
        self.createFullView()
        self.createTextSearch()
        self.createDMCsearch()
        self.createToilesearch()
        self.createVendeursearch()
        self.createDMCVendeursearch()

        self.tabBar.pack(expand=1, fill='both')

    def createImp(self):
        #self.tabImp = ttk.Frame(self.tabBar)
        #self.tabBar.add(self.tabImp, text="Import")

        self.BoutonImport = tk.Button(self, text="Import", command=self.askFileImport)
        self.BoutonImport.pack(side = tk.LEFT)

    def createExp(self):
        #self.tabExp = ttk.Frame(self.tabBar)
        #self.tabBar.add(self.tabExp, text="Export")

        self.BoutonExport = tk.Button(self, text="Export", command=self.askFileExport)
        self.BoutonExport.pack(side=tk.LEFT)

    def createAjout(self):
        self.tabAjout = ttk.Frame(self.tabBar)
        self.tabBar.add(self.tabAjout, text="Ajout DMC")

        self.DMCLabel = tk.Label(self.tabAjout, text="DMC:")
        self.DMCEntry = tk.Entry(self.tabAjout, width=6)
        self.DMCLabel.grid(row=0, column=0)
        self.DMCEntry.grid(row=0, column=1)
        self.ToileLabel = tk.Label(self.tabAjout, text="Toile:")
        self.ToileEntry = tk.Entry(self.tabAjout, width=10)
        self.ToileLabel.grid(row=1, column=0)
        self.ToileEntry.grid(row=1, column=1)
        self.VendeurLabel = tk.Label(self.tabAjout, text="Vendeur:")
        self.VendeurEntry = tk.Entry(self.tabAjout, width=10)
        self.VendeurLabel.grid(row=2, column=0)
        self.VendeurEntry.grid(row=2, column=1)
        self.DateLabel = tk.Label(self.tabAjout, text="Date:")
        self.DateEntry = tk.Entry(self.tabAjout, width=10)
        self.DateEntry.insert(0, date.strftime(date.today(), "%d/%m/%Y"))
        self.DateLabel.grid(row=3, column=0)
        self.DateEntry.grid(row=3, column=1)

        self.AjoutValiderButton = tk.Button(self.tabAjout, text="Valider", command=self.ajoutValider)
        self.AjoutValiderButton.grid(row=4, column=1)


    def createFullView(self):
        self.tabFullView = ttk.Frame(self.tabBar)
        self.tabBar.add(self.tabFullView, text="Vue complète")

        self.tabFullSearchButton = tk.Button(self, text="Recherche tout", command=self.dispFull)
        self.tabFullSearchButton.pack(side = tk.LEFT)

    def createTextSearch(self):
        self.DmcSearchLabel = tk.Label(self, text="DMC:")
        self.DmcSearchEntry = tk.Entry(self, width=6)
        self.DmcSearchLabel.pack(side=tk.LEFT)
        self.DmcSearchEntry.pack(side=tk.LEFT)
        self.ToileSearchLabel = tk.Label(self, text="Toile:")
        self.ToileSearchEntry = tk.Entry(self, width=10)
        self.ToileSearchLabel.pack(side=tk.LEFT)
        self.ToileSearchEntry.pack(side=tk.LEFT)
        self.VendSearchLabel = tk.Label(self, text="Vendeur:")
        self.VendSearchEntry = tk.Entry(self, width=10)
        self.VendSearchLabel.pack(side=tk.LEFT)
        self.VendSearchEntry.pack(side=tk.LEFT)

    def createDMCsearch(self):
        self.tabDMCsearch = ttk.Frame(self.tabBar)
        self.tabBar.add(self.tabDMCsearch, text="DMC")

        self.tabDMCsearchButton = tk.Button(self, text="Rech. DMC", command=self.dispDMC)
        self.tabDMCsearchButton.pack(side=tk.LEFT)

    def createToilesearch(self):
        self.tabToilesearch = ttk.Frame(self.tabBar)
        self.tabBar.add(self.tabToilesearch, text="Toile")

        self.tabToilesearchButton = tk.Button(self, text="Rech. Toile", command=self.dispToile)
        self.tabToilesearchButton.pack(side=tk.LEFT)

    def createVendeursearch(self):
        self.tabVendsearch = ttk.Frame(self.tabBar)
        self.tabBar.add(self.tabVendsearch, text="Vendeur")

        self.tabVendsearchButton = tk.Button(self, text="Rech. Vendeur", command=self.dispVendeur)
        self.tabVendsearchButton.pack(side=tk.LEFT)

    def createDMCVendeursearch(self):
        self.tabDMCVendsearch = ttk.Frame(self.tabBar)
        self.tabBar.add(self.tabDMCVendsearch, text="DMC+Vendeur")

        self.tabDMCVendsearchButton = tk.Button(self, text="Rech. DMC+Vendeur", command=self.dispDMCVendeur)
        self.tabDMCVendsearchButton.pack(side=tk.LEFT)

    def dispFull(self):
        fullData = self.bdd.formatForGrid(range(0, self.bdd.getNbRefs()))
        print(fullData)
        for i in range(len(fullData)):
            for j in range(4):
                self.e = tk.Entry(self.tabFullView, fg='blue', font=('Arial', 10, 'bold'))
                self.e.grid(row=i, column=j)
                self.e.insert(tk.END, fullData[i][j])

    def dispDMC(self):
        recherche = self.DmcSearchEntry.get()
        print(recherche)
        listIndex = self.bdd.searchDMC(recherche)
        print(listIndex)
        data = self.bdd.formatForGrid(listIndex)
        print(data)
        for i in range(len(data)):
            for j in range(4):
                self.e = tk.Entry(self.tabDMCsearch, fg='blue', font=('Arial', 10, 'bold'))
                self.e.grid(row=i, column=j)
                self.e.insert(tk.END, data[i][j])

    def dispToile(self):
        recherche = self.ToileSearchEntry.get()
        print(recherche)
        listIndex = self.bdd.searchToile(recherche)
        print(listIndex)
        data = self.bdd.formatForGrid(listIndex)
        print(data)
        for i in range(len(data)):
            for j in range(4):
                self.e = tk.Entry(self.tabToilesearch, fg='blue', font=('Arial', 10, 'bold'))
                self.e.grid(row=i, column=j)
                self.e.insert(tk.END, data[i][j])

    def dispVendeur(self):
        recherche = self.VendSearchEntry.get()
        print(recherche)
        listIndex = self.bdd.searchVendeur(recherche)
        print(listIndex)
        data = self.bdd.formatForGrid(listIndex)
        print(data)
        for i in range(len(data)):
            for j in range(4):
                self.e = tk.Entry(self.tabVendsearch, fg='blue', font=('Arial', 10, 'bold'))
                self.e.grid(row=i, column=j)
                self.e.insert(tk.END, data[i][j])

    def dispDMCVendeur(self):
        rechercheDMC = self.DmcSearchEntry.get()
        rechercheVend = self.VendSearchEntry.get()
        listIndex = self.bdd.searchDMCVendeur(rechercheDMC, rechercheVend)
        print(listIndex)
        if self.e.winfo_exists():
            print("Ho")
            self.e.grid_remove()
            self.e.destroy()
        if listIndex != []:
            data = self.bdd.formatForGrid(listIndex)
            print(data)
            for i in range(len(data)):
                for j in range(4):
                    self.e = tk.Entry(self.tabDMCVendsearch, fg='blue', font=('Arial', 10, 'bold'))
                    self.e.grid(row=i, column=j)
                    self.e.insert(tk.END, data[i][j])
        else:
            self.e = tk.Label(self.tabDMCVendsearch, text="Aucun résultat")
            self.e.grid_remove()
            self.e.grid(row=0, column=0)
            # for j in range(4):
            #     self.e = tk.Entry(self.tabDMCVendsearch, fg='blue', font=('Arial', 10, 'bold'))
            #     self.e.grid(row=1, column=j)
            #     self.e.insert(tk.END, "")

    def ajoutValider(self):
        myDMC = self.DMCEntry.get()
        myToile = self.ToileEntry.get()
        myVendeur = self.VendeurEntry.get()
        myDate = self.DateEntry.get()
        if myDMC == "":
            self.errMsg = tk.Label(self.tabAjout, text="DMC manquant")
            self.errMsg.grid(row=5, column=1)
        elif myToile == "":
            self.errMsg = tk.Label(self.tabAjout, text="Toile manquante")
            self.errMsg.grid(row=5, column=1)
        elif myVendeur == "":
            self.errMsg = tk.Label(self.tabAjout, text="Vendeur manquant")
            self.errMsg.grid(row=5, column=1)
        elif myDate == "":
            self.errMsg = tk.Label(self.tabAjout, text="Date manquante")
            self.errMsg.grid(row=5, column=1)
        else:
            myDMCtoAdd = DMC()
            myDMCtoAdd.setDMC(myDMC)
            myDMCtoAdd.setToile(myToile)
            myDMCtoAdd.setVendeur(myVendeur)
            myDMCtoAdd.setDateCreation(myDate)
            self.bdd.addDMC(myDMCtoAdd)

            self.errMsg = tk.Label(self.tabAjout, text="DMC ajouté !")
            self.errMsg.grid(row=5, column=1)

    def say_hi(self):
        print("hi there, everyone!")

    def askFileImport(self):
        fileName = filedialog.askopenfilename(initialdir="./", title="Choisir un fichier", filetypes=(("csv files", "*.csv"), ("all files", "*.*")))
        self.bdd.importDB(fileName)
        print(self.bdd.getDMC(0).getToile())

    def askFileExport(self):
        fileName = filedialog.asksaveasfilename(initialdir="./", title="Choisir un fichier", filetypes=(("csv files", "*.csv"), ("all files", "*.*")))
        self.bdd.writeDB(fileName)
root = tk.Tk()
root.title("DMC-db")
root.geometry("800x800")
app = Application(master=root)
app.mainloop()
