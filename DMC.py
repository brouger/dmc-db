"""Ce fichier défini la classe DMC, qui contient les informaitons liées à un lot de diamands et sa couleur"""


from datetime import date

class DMC:
    def __init__(self):
        self.dmc = None
        self.toile = None
        self.vendeur = None
        self.dateCreation = None

    def setDMC(self, valDMC):
        self.dmc = str(valDMC)

    def getDMC(self):
        return self.dmc

    def setToile(self, valToile):
        self.toile = valToile

    def getToile(self):
        return self.toile

    def setVendeur(self, valVendeur):
        self.vendeur = valVendeur

    def getVendeur(self):
        return self.vendeur

    def setDateCreation(self, valDate):
        dateSplt = valDate.split("/")
        if dateSplt[0][0] == "0":
            jour = dateSplt[0][1]
        else:
            jour = dateSplt[0]
        if dateSplt[1][0] == "0":
            mois = dateSplt[1][1]
        else:
            mois = dateSplt[1]
        if int(dateSplt[2]) < 1990 or len(dateSplt[2]) == 2:
            annee = "20" + dateSplt[2]
        else:
            annee = dateSplt[2]
        myDate = date(int(annee), int(mois), int(jour))
        self.dateCreation = myDate

    def getDateCreation(self):
        return self.dateCreation
