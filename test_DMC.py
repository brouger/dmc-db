from DMC import *
from datetime import date

def test_setGetDMC():
    dmc1 = DMC()
    dmc1.setDMC('A123')
    assert dmc1.getDMC() == 'A123'
    dmc2 = DMC()
    dmc2.setDMC(123)
    assert dmc2.getDMC() == "123"

def test_setGetToile():
    dmc1 = DMC()
    dmc1.setToile("Yoda")
    assert dmc1.getToile() == "Yoda"

def test_setGetVendeur():
    dmc1 = DMC()
    dmc1.setVendeur("Manhui")
    assert dmc1.getVendeur() == "Manhui"

def test_setGetDateCreation():
    dmc1 = DMC()
    a = "10/11/2021"
    dmc1.setDateCreation(a)
    assert dmc1.getDateCreation() == date(2021, 11, 10)
    a = "10/01/2021"
    dmc1.setDateCreation(a)
    assert dmc1.getDateCreation() == date(2021, 1, 10)
    a = "10/11/21"
    dmc1.setDateCreation(a)
    assert dmc1.getDateCreation() == date(2021, 11, 10)
    a = "10/01/21"
    dmc1.setDateCreation(a)
    assert dmc1.getDateCreation() == date(2021, 1, 10)
    a = "03/01/21"
    dmc1.setDateCreation(a)
    assert dmc1.getDateCreation() == date(2021, 1, 3)
