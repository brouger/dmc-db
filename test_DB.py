from DB import *
from DMC import *
from datetime import date
import hashlib
import os

def createDMC1():
    dmc1 = DMC()
    dmc1.setDMC('A123')
    dmc1.setToile("Yoda")
    dmc1.setVendeur("Manhui")
    a = "10/11/2021"
    dmc1.setDateCreation(a)
    return dmc1

def createDMC2():
    dmc2 = DMC()
    dmc2.setDMC(123)
    dmc2.setToile("Rose")
    dmc2.setVendeur("Homfun")
    a = "10/01/2021"
    dmc2.setDateCreation(a)
    return dmc2

def createDMC3():
    dmc3 = DMC()
    dmc3.setDMC("A123")
    dmc3.setToile("Rose")
    dmc3.setVendeur("Manhui")
    a = "10/01/2021"
    dmc3.setDateCreation(a)
    return dmc3

def createDB():
    a = createDMC1()
    b = createDMC2()

    myDB = DB()
    myDB.addDMC(a)
    myDB.addDMC(b)
    return myDB

def md5Checksum(filePath):
    with open(filePath, 'rb') as fh:
        m = hashlib.md5()
        while True:
            data = fh.read(8192)
            if not data:
                break
            m.update(data)
        return m.hexdigest()

def test_addRmGetDMC():
    myDB = createDB()

    assert myDB.getDMC(0).getDMC() == "A123"
    assert myDB.getDMC(1).getToile() == "Rose"

    myDB.rmDMC(0)

    assert myDB.getDMC(0).getDMC() == "123"

def test_getNbRefs():
    myDB = createDB()
    assert myDB.getNbRefs() == 2
    myDB.addDMC(createDMC3())
    assert myDB.getNbRefs() == 3
    myDB.rmDMC(2)
    assert myDB.getNbRefs() == 2

def test_exportDB():
    myDB = createDB()

    assert myDB.exportDB() == "A123;Yoda;Manhui;2021-11-10\n123;Rose;Homfun;2021-01-10\n"

def test_writeDB():
    myDB = createDB()

    myDB.writeDB("./test.csv")
    assert md5Checksum("./test.csv") == md5Checksum("./testWrite1.csv")
    os.remove("./test.csv")

def test_importDB():
    myDB = DB()
    myDB.importDB("./testWrite1.csv")
    myDB.writeDB("./test.csv")
    assert md5Checksum("./test.csv") == md5Checksum("./testWrite1.csv")
    assert myDB.getNbRefs() == 2
    os.remove("./test.csv")

def test_searchDMC():
    myDB = createDB()
    assert myDB.searchDMC("A123") == [0]

    myDB.addDMC(createDMC3())
    assert myDB.searchDMC("A123") == [0, 2]

def test_searchToile():
    myDB = createDB()
    assert myDB.searchToile("Rose") == [1]

    myDB.addDMC(createDMC3())
    assert myDB.searchToile("Rose") == [1, 2]

def test_searchVendeur():
    myDB = createDB()
    assert myDB.searchVendeur("Manhui") == [0]

    myDB.addDMC(createDMC3())
    assert myDB.searchVendeur("Manhui") == [0, 2]

def test_searchDMCVendeur():
    myDB = createDB()
    assert myDB.searchDMCVendeur("A123", "Manhui") == [0]
    myDB.addDMC(createDMC3())
    assert myDB.searchDMCVendeur("A123", "Manhui") == [0, 2]

def test_formatForGrid():
    myDB = createDB()

    a = [["A123", "Yoda", "Manhui", "10/11/2021"], ["123", "Rose", "Homfun", "10/01/2021"]]
    assert myDB.formatForGrid(range(myDB.getNbRefs())) == a

    b = [["123", "Rose", "Homfun", "10/01/2021"]]
    assert myDB.formatForGrid(1) == b

    c = []
    assert myDB.formatForGrid(3) == c
