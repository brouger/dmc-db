"""Ce fichier défini la classe de la BDD contenant les DMCs"""

from datetime import date
from DMC import *

class DB:
    def __init__(self):
        self.db = []
        self.nbRefs = 0

    def addDMC(self, myDMC):
        self.db.append(myDMC)
        self.nbRefs += 1

    def getDMC(self, index):
        return self.db[index]

    def rmDMC(self, index):
        del self.db[index]
        self.nbRefs -= 1

    def getNbRefs(self):
        return self.nbRefs

    def exportDB(self):
        linesToWrite = ""
        for d in self.db:
            infos = [str(d.getDMC()), str(d.getToile()),
                     str(d.getVendeur()), str(d.getDateCreation())]
            ligne = ";".join(infos)
            linesToWrite += ligne + "\n"
        return linesToWrite

    def writeDB(self, chemin):
        f = open(chemin, "w")
        f.write(self.exportDB())
        f.close()

    def importDB(self, chemin):
       f = open(chemin, "r")
       g = f.read()
       f.close()
       g = g.strip()
       gList = g.split("\n")
       self.DB = []
       self.nbRefs = len(gList)
       for i in gList:
           dmcList = i.split(";")
           newDMC = DMC()
           newDMC.setDMC(dmcList[0])
           newDMC.setToile(dmcList[1])
           newDMC.setVendeur(dmcList[2])
           dateTemp = dmcList[3].split("-")
           dateStr = dateTemp[2] + "/" + dateTemp[1] + "/" + dateTemp[0]
           newDMC.setDateCreation(dateStr)
           self.db.append(newDMC)

    def searchDMC(self, string):
        indicesCorrespondants = []
        for i in range(0, len(self.db)):
            if self.db[i].getDMC() == string:
                indicesCorrespondants.append(i)
        return indicesCorrespondants

    def searchToile(self, string):
        indicesCorrespondants = []
        for i in range(0, len(self.db)):
            if self.db[i].getToile() == string:
                indicesCorrespondants.append(i)
        return indicesCorrespondants

    def searchVendeur(self, string):
        indicesCorrespondants = []
        for i in range(0, len(self.db)):
            if self.db[i].getVendeur() == string:
                indicesCorrespondants.append(i)
        return indicesCorrespondants

    def searchDMCVendeur(self, string1, string2):
        indicesCorrespondants = []
        for i in range(0, len(self.db)):
            if self.db[i].getDMC() == string1 and self.db[i].getVendeur() == string2:
                indicesCorrespondants.append(i)
        return indicesCorrespondants

    def formatForGrid(self, indicesList):
        print(indicesList)
        listToReturn = []
        if type(indicesList) == int:
            indicesList = [indicesList]
            print("ha")
        if indicesList == []:
            return []
        if max(indicesList) > self.nbRefs:
            print("ho")
            return []
        if (len(indicesList) >= 1):
            for i in indicesList:
                print(i)
                myDmc = self.db[i].getDMC()
                myToile = self.db[i].getToile()
                myVendeur = self.db[i].getVendeur()
                myDate = date.strftime(self.db[i].getDateCreation(),
                                       "%d/%m/%Y")
                listToReturn.append([myDmc, myToile, myVendeur, myDate])
            return listToReturn
